import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { hello } from '@harrisejm/test';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.sass'
})
export class AppComponent {
  title = 'hello';

  ngOnInit() {
    new hello().test1();
    new hello().test2();
  }
}
